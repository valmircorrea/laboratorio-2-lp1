/**
* @file palindromo.h
* @brief Arquivo com cabeçário da função palindromo.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/

#include <cstring>
using std::string;

#ifndef BUSCA_TERNARIA_H
#define BUSCA_TERNARIA_H

/**
* @brief Verifica se os primeiros caracteres da palavra são iguais aos ultimos.
* @param palavra Recebe a string.
* @param inicio Indice de onde está o primeiro caractere da string. 
* @param fim Indice de onde está o ultimo caractere da string.
* @return Retorna "true" ou "false" se a palavra for ou não um palindromo.
*/
bool palindromo ( string palavra, int inicio, int fim );

#endif