/**
* @file main6.cpp
* @brief Arquivo com a função principal.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <iostream> 
#include <cstdlib>

#include "busca_ternaria.h"

using std:: cout;
using std:: cin;
using std:: endl;

/**
*@brief Função principal que recebe o elemento para a busca e imprime uma mensagem dizendo se elemento está ou não contido no vetor.
*/
int main (int agrc, char* argv[]) {

    int vetor[26] = { 2,5,9,11,13,17,22,24,33,38,39,40,45,56,71,99,110,113,132,155,166,203,211,212,230,233 };

    int chave = atoi(argv[1]);

    if (busca_ternaria (vetor, 0, 25, chave ) == 0) {
        cout << "O elemento " << chave << " não faz parte do vetor" << endl;
    }
    else {
        cout << "O elemento " << chave << " faz parte do vetor" << endl;
    }

    return 0;
}