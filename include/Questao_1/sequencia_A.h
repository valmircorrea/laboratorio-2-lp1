/**
* @file sequencia_A.h
* @brief Arquivo com o cabeçário das funções da sequencia_A.cpp
* @author Valmir Correa
* @since 17/03/17
* @date 19/03/17
*/
#ifndef SEQUENCIA_A_H
#define SEQUENCIA_A_H

/**
* @brief Realiza o cálculo da sequência do tipo A ( 1 + 1/2 + 1/3 + 1/4 ... + 1/N ) na forma recursiva.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_A_Recursiva (float N );

/**
* @brief Realiza o cálculo da sequência do tipo A ( 1 + 1/2 + 1/3 + 1/4 ... + 1/N ) na forma iterativa.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_A_Iterativa (float N);

#endif
