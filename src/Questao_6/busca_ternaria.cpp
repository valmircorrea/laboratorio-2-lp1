/**
* @file busca_ternaria.cpp
* @brief Arquivo com a função busca_ternaria.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include "busca_ternaria.h"

/**
* @brief Realiza a busca do elemento no vetor.
* @details Algoritimo estudado na disciplina de EDB 1.
* @param v Recebe o vetor.
* @param ini Recebe o índice do inicio do vetor.
* @param fim Recebe o índice do fim do vetor.
* @param x Recebe o elemento a ser buscado no vetor.
* @return Retorna o numero no formato binário ("1" se estiver ou "0" se não.).
*/
bool busca_ternaria(int *v, int ini, int fim, int x) {

    if (fim < ini) {
        return false;
    }

    int mid1 = ini + ((fim - ini)/3);
    int mid2 = ini + (2 *(fim - ini)/3);

    if (v[mid1] == x) {
        return true;
    }
    else if (v[mid2] == x) {
        return true;
    }
    else if (v[mid1] < x) {
        return busca_ternaria (v,mid1+1, fim, x);
    }
    else if (v[mid2] > x) {
        return busca_ternaria (v, ini, mid2-1, x);
    }
    else {
        return busca_ternaria (v, mid1+1, mid2-1, x);
    }
    return false;
}