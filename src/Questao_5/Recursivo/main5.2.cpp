/**
* @file main5.2.cpp
* @brief Arquivo com a função principal.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <iostream> 
using std:: cout;
using std:: endl;

#include <cstdlib>

#include "quadrado_de_n.h"
#include "imprime.h"

/**
*@brief Função príncipal que recebe um número para calcular seu quadrado com a soma de todos os
* números ímpares inferiores ao dobro do número.
*/
int main (int agrc, char* argv[]) {
    
    Imprime_numeros (atoi(argv[1]));
    cout << " = " << Quadrado_de_um_num (atoi(argv[1])) << endl;

    return 0;
}