/**
* @file main4.cpp
* @brief Arquivo com a função principal.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <iostream> 
using std:: cout;
using std:: cin;
using std:: endl;

#include <cstring>
using std::string;

#include "palindromo.h"

/**
*@brief Função príncipal que lê uma palavra e envia a mensagem ao usuário se a palavra é uma palindromo ou não.
*/
int main () {

    string palavra;

    cout << "Digite uma palavra: ";
    getline(cin, palavra); /** < função "getline" para ler a entrada */

    int tamanho = palavra.length(); /** < função "length" que atribui o tamanho da string */
    
    if (palindromo (palavra, 0, tamanho-1) == true) {
        cout << "'" << palavra << "'" << " é um palindromo" << endl;
    }
    else {
        cout << "'" << palavra << "'" << " não é um palindromo" << endl;
    }

    return 0;
}