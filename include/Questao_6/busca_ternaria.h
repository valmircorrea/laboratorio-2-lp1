/**
* @file busca_ternaria.h
* @brief Arquivo com o cabeçário da função "busca_ternaria"
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#ifndef BUSCA_TERNARIA_H
#define BUSCA_TERNARIA_H

/**
* @brief Realiza a busca do elemento no vetor.
* @details Algoritimo estudado na disciplina de EDB 1.
* @param v Recebe o vetor.
* @param ini Recebe o índice do inicio do vetor.
* @param fim Recebe o índice do fim do vetor.
* @param x Recebe o elemento a ser buscado no vetor.
* @return Retorna o numero no formato binário ("1" se estiver ou "0" se não.).
*/
bool busca_ternaria(int *v, int ini, int fim, int x);

#endif