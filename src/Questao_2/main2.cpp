/**
* @file main2.cpp
* @brief Arquivo com a função principal.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <iostream> 

#include "calculo_MDC.h"

using std:: cout;
using std:: cin;
using std:: endl;

/**
*@brief Função principal que lê 2 numeros passados pelo usuário e gera a saída do MDC entre eles.
*/
int main () {

    int m, n;
    cout << "Digite dois numeros naturais positivos: ";
    cin >> m >> n;
    if (m <= 0 || n <= 0) { /**< Condição para que o usuário não passe valores negativos */
        cout << endl << "Parâmetro(s) inválido(s)! Digite apenas valores maiores que 0!" << endl;
    }
    else {
        cout << "MDC(" << m << "," << n << ") = " << Calculo_MDC (m, n) << endl;
    }
 
    return 0;
}