/**
* @file main1.cpp
* @brief Arquivo com a função principal.
* @author Valmir Correa
* @since 17/03/17
* @date 19/03/17
*/
#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;

#include <cstdlib>
#include <cstring>

#include "sequencia_A.h"
#include "sequencia_B.h"

/**
*@brief Função principal que lê os parâmetros do terminal e imprime o resultado de acordo com a escolha do usuário.
*/
int main (int argc, char *argv[]) {

    std::cout.precision(3);

    if (strcmp (argv[1],"A") == 0) {

        if (strcmp (argv[2],"R") == 0) {

            cout << "O Valor da sequência A para N = " << atoi(argv[3]) << " e " << Sequencia_A_Recursiva (atoi(argv[3]))
            << " (a versão recursiva foi usada)" << endl;
        }

        else if (strcmp (argv[2],"I") == 0) {

            cout << "O Valor da sequência A para N = " << atoi(argv[3]) << " e " << Sequencia_A_Iterativa (atoi(argv[3]))
            << " (a versão iterativa foi usada)" << endl;

        }

        else {

            cout << "Parâmetro inválido!" << endl;

        }

    }

    else if (strcmp (argv[1],"B") == 0) {

        if (strcmp (argv[2],"R") == 0) {

            cout << "O Valor da sequência B para N = " << atoi(argv[3]) << " é " << Sequencia_B_Recursiva (atoi(argv[3]))
            << " (a versão recursiva foi usada)" << endl;
        }

        else if (strcmp (argv[2],"I") == 0) {

            cout << "O Valor da sequência B para N = " << atoi(argv[3]) << " é " << Sequencia_B_Iterativa (atoi(argv[3]))
            << " (a versão iterativa foi usada)" << endl;
        }

        else {

            cout << "Parâmetro inválido!" << endl;

        }
    }

    else {

        cout << "Parâmetro inválido!" << endl;

    }

    return 0;
}
