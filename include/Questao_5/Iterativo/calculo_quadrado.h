/**
* @file calculo_quadrado.h
* @brief Arquivo com o cabeçário das funções do calculo_quadrado.cpp
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#ifndef CALCULO_QUADRADO_H
#define CALCULO_QUADRADO_H

/**
* @brief Realiza o cálculo de n².
* @param n Recebe o número para calcular seu quadrado.
* @return Retorna o resultado de n² e imprime os valores da soma de todos os números ímpares inferiores ao dobro de n.
*/
void Quadrado_de_um_num_iterativo (int n);

#endif
