/**
* @file sequencia_A.cpp
* @brief Arquivo com as funções da sequencia A.
* @author Valmir Correa
* @since 17/03/17
* @date 19/03/17
*/
#include "sequencia_A.h"

/**
* @brief Realiza o cálculo da sequência do tipo A ( 1 + 1/2 + 1/3 + 1/4 ... + 1/N ) na forma recursiva.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_A_Recursiva (float N ) {
    if (N == 1) {
        return 1;
    }

    else {
        return  1/N + Sequencia_A_Recursiva (N-1);
    }   
}

/**
* @brief Realiza o cálculo da sequência do tipo A ( 1 + 1/2 + 1/3 + 1/4 ... + 1/N ) na forma iterativa.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_A_Iterativa (float N) {

    float result = 0;
    for (float i = 1; i <= N; i++) {
        result += 1/i;
    }
    
    return result;
}
