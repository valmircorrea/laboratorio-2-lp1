/**
* @file sequencia_B.cpp
* @brief Arquivo com as funções da sequencia B.
* @author Valmir Correa
* @since 17/03/17
* @date 19/03/17
*/
#include <cmath>
#include <iostream> 
using std:: cout;

#include "sequencia_B.h"

/**
* @brief Realiza o cálculo da sequência do tipo B ( 2/4 + 5/5 + 10/6 + 17/7 ... + (N²+1/N+3) ) na forma recursiva.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_B_Recursiva (float N) {

    if (N == 0) {
        return 0;
    }
    else {
        return ( pow(N,2) + 1 ) / (N + 3 ) + Sequencia_B_Recursiva (N-1);
    }
}

/**
* @brief Realiza o cálculo da sequência do tipo B ( 2/4 + 5/5 + 10/6 + 17/7 ... + (N²+1/N+3) ) na forma Iterativa.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_B_Iterativa (float N ) {

    float result = 0, aux, j = 1;

    for (float i = 1; i <= N; i++) {
        aux = (pow (j,2)) + 1;
        result += aux/(3+i);
        j++;
    }
    return result ;
}

