/**
* @file quadrado_de_n.h
* @brief Arquivo com o cabeçário das funções do quadrado_de_n.cpp
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#ifndef QUADRADO_DE_N_H
#define QUADRADO_DE_N_H

/**
* @brief Realiza o cálculo de N².
* @param N Recebe o número para calcular seu quadrado.
* @return Retorna o valor de N².
*/
int Quadrado_de_um_num (int N);

#endif