/**
* @file calculo_MDC.cpp
* @brief Arquivo com a função de cálculo do MDC.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include "calculo_MDC.h"

/**
* @brief Realiza o cálculo do MDC entre dois números.
* @param m Recebe um dos valores para o cáculo.
* @param n Recebe um dos valores para o cáculo.
* @return Retorna o valor do MDC entre "m" e "n".
*/
int Calculo_MDC (int m, int n) {
    if (n == 0) {
        return m;
    }
    else {
        return Calculo_MDC (n, m % n);
    }
}