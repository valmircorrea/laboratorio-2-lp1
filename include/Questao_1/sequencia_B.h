/**
* @file sequencia_B.h
* @brief Arquivo com o cabeçário das funções da sequencia_B.cpp
* @author Valmir Correa
* @since 17/03/17
* @date 19/03/17
*/
#ifndef SEQUENCIA_B_H
#define SEQUENCIA_B_H

/**
* @brief Realiza o cálculo da sequência do tipo B ( 2/4 + 5/5 + 10/6 + 17/7 ... + (N²+1/N+3) ) na forma recursiva.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_B_Recursiva (float N);

/**
* @brief Realiza o cálculo da sequência do tipo B ( 2/4 + 5/5 + 10/6 + 17/7 ... + (N²+1/N+3) ) na forma Iterativa.
* @param N Recebe a quantidade da sequência para o somatório.
* @return Retorna o valor do somatório.
*/
float Sequencia_B_Iterativa (float N);

#endif
