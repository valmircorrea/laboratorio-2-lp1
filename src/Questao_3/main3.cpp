/**
* @file main3.cpp
* @brief Arquivo com a função principal.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <iostream> 

#include "conversao_binaria.h"

using std:: cout;
using std:: cin;
using std:: endl;

/**
*@brief Função principal que solicita a entrada de um numero na base 10 e imprime seu correspondente em binário.
*/
int main () {

    int num;
    cout << "Digite um numero não-negativo: ";
    cin >> num;;
    if (num < 0) { /**< Condição para que o usuário não passe valores negativos */
        cout << endl << "Parâmetro inválido! Digite um numero maior ou igual a 0!" << endl;
    }
    else {
        cout << "Representação de " << num << " na forma binaria: " << Conversao_Binaria (num) << endl;
    }

    return 0;
}