/**
* @file palindromo.cpp
* @brief Arquivo com a função palindromo.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <iostream> 
#include <cstring>
using std::string;

#include <cctype>

#include "palindromo.h"

/**
* @brief Verifica se os primeiros caracteres da palavra são iguais aos ultimos.
* @param palavra Recebe a string.
* @param inicio Indice de onde está o primeiro caractere da string. 
* @param fim Indice de onde está o ultimo caractere da string.
* @return Retorna "true" ou "false" se a palavra for ou não um palindromo.
*/
bool palindromo ( string palavra, int inicio, int fim ) {

    char letra = palavra[inicio];
    char letra2 = palavra[fim];

    if ( tolower(letra) != tolower(letra2) )  { /** < função "tolower" usada para transformar todas as letras para minúsculas */
        return false;
    }

    if (fim <= inicio) {
        return true;
    }

    else {
        return palindromo (palavra, inicio+1, fim-1);
    }
}
