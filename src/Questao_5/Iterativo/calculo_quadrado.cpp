/**
* @file calculo_quadrado.cpp
* @brief Arquivo com a função Quadrado_de_um_num_iterativo.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <cmath>
#include <iostream> 
using std:: cout;
using std:: endl;

#include "calculo_quadrado.h"

/**
* @brief Realiza o cálculo de n².
* @param n Recebe o número para calcular seu quadrado.
* @return Retorna o resultado de n² e imprime os valores da soma de todos os números ímpares inferiores ao dobro de n.
*/
void Quadrado_de_um_num_iterativo (int n) {

    cout << "Quadrado (" << n << ") => ";

    int result = 0, result2 = 0;
    
    for (int i = 1; i <= n ; i++) {

        result = ( (2*i) - 1);
        result2 += result;
        
        if (result2 != pow(n,2)) {

            cout << result << " + ";
        }

        else if (result2 == pow(n,2)) {
            cout << result;
            cout << " = " << result2 << endl;
        }
    }
}
