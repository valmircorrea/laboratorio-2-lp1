/**
* @file main5.1.cpp
* @brief Arquivo com a função principal.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <iostream> 
using std:: cout;
using std:: endl;

#include <cstdlib>

#include "calculo_quadrado.h"

/**
*@brief Função príncipal que recebe um número para calcular seu quadrado com a soma de todos os
* números ímpares inferiores ao dobro do número.
*/
int main (int agrc, char* argv[]) {
    
    Quadrado_de_um_num_iterativo(atoi(argv[1]));

    return 0;
}
