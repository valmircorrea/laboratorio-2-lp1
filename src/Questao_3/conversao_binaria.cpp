/**
* @file conversao_binaria.cpp
* @brief Arquivo com a função de conversão.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include "conversao_binaria.h"

/**
* @brief Realiza a conversão de um numero na base 10 para seu correspondente na base 2.
* @param num Recebe o numero a ser convertido.
* @return Retorna o numero no formato binário.
*/
int Conversao_Binaria (int num) {
    if (num < 2) {
        return num;
    }

    return (10 * Conversao_Binaria (num / 2) + num % 2); 

}