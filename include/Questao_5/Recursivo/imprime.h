/**
* @file imprime.h
* @brief Arquivo com o cabeçário da função de imprime.cpp
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#ifndef IMPRIME_H
#define IMPRIME_H

/**
* @brief Realiza o cálculo de N², para poder imprimir a soma dos numeros ímpares inferiores ao dobro de N.
* @param N Recebe o número para calcular seu quadrado.
*/
void Imprime_numeros (int n);

#endif