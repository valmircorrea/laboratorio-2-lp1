/**
* @file conversao_binaria.h
* @brief Arquivo com o cabeçário da função "Conversao_Binaria".
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#ifndef CONVERSAO_BINARIA_H
#define CONVERSAO_BINARIA_H

/**
* @brief Realiza a conversão de um numero na base 10 para seu correspondente na base 2.
* @param num Recebe o numero a ser convertido.
* @return Retorna o numero no formato binário.
*/
int Conversao_Binaria (int num);

#endif