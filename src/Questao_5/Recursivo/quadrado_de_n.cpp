/**
* @file quadrado_de_n.cpp
* @brief Arquivo com a função Quadrado_de_um_num.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include "quadrado_de_n.h"

/**
* @brief Realiza o cálculo de N².
* @param N Recebe o número para calcular seu quadrado.
* @return Retorna o valor de N².
*/
int Quadrado_de_um_num (int N) {

    if (N == 1) {
        return 1;
    }
    else {
        return (2*N-1) + Quadrado_de_um_num (N-1);
    }
    
    return 0;
}