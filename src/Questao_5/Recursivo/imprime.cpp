/**
* @file imprime.cpp
* @brief Arquivo com a função Imprime_numeros.
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#include <cmath>
#include <iostream>
using std:: cout;
using std:: endl;

#include "imprime.h"

/**
* @brief Realiza o cálculo de N², para poder imprimir a soma dos números ímpares inferiores ao dobro de N.
* @param N Recebe o número para calcular seu quadrado.
*/
void Imprime_numeros (int n) {
    
    cout << "Quadrado (" << n << ") => ";
    int result = 0, result2 = 0;
    for (int i = 1; i <= n ; i++){
        result = ( (2*i) - 1);
        result2 += result;
        if (result2 != pow(n,2)) {
            cout << result << " + ";
        }
        else {
            cout << result;
        }
    }
}