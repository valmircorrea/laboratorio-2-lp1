/**
* @file calculo_MDC.h
* @brief Arquivo com o cabeçário da função "Calculo_MDC".
* @author Valmir Correa
* @since 17/03/17
* @date 17/03/17
*/
#ifndef calculo_MDC_H
#define calculo_MDC_H

/**
* @brief Realiza o cálculo do MDC entre dois números.
* @param m Recebe um dos valores para o cáculo.
* @param n Recebe um dos valores para o cáculo.
* @return Retorna o valor do MDC entre "m" e "n".
*/
int Calculo_MDC (int m, int n);

#endif