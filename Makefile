#Makefile for "laboratorio-02" C++ application
#Created by Valmir Correa 19/03/2017

RM = rm -rf

# Compilador:
CC = g++

# Variaveis para os subdiretorios:

INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)/Questao_1 -I$(INC_DIR)/Questao_2 -I$(INC_DIR)/Questao_3 -I$(INC_DIR)/Questao_4 -I$(INC_DIR)/Questao_5/Iterativo -I$(INC_DIR)/Questao_5/Recursivo -I$(INC_DIR)/Questao_6

# Assegura que os alvos não sejam confundidos com os arquivos de mesmo nome:
.PHONY: all clean distclean doxy

# Define o alvo para a compilação completa:
all: sequencia mdc dec2bin palindromo quadrado_iterativo quadrado_recursivo ternaria

debug: CFLAGS += -g -O0
debug: sequencia mdc dec2bin palindromo quadrado_iterativo quadrado_recursivo ternaria

# Alvo para a construcao do executavel Questao_1:
sequencia: $(OBJ_DIR)/main1.o $(OBJ_DIR)/sequencia_A.o $(OBJ_DIR)/sequencia_B.o
	
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construcao do objeto main1.o:
$(OBJ_DIR)/main1.o: $(SRC_DIR)/Questao_1/main1.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto sequencia_A.o:
$(OBJ_DIR)/sequencia_A.o: $(SRC_DIR)/Questao_1/sequencia_A.cpp $(INC_DIR)/Questao_1/sequencia_A.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto sequencia_B.o:
$(OBJ_DIR)/sequencia_B.o: $(SRC_DIR)/Questao_1/sequencia_B.cpp $(INC_DIR)/Questao_1/sequencia_B.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do executavel Questao_2:
mdc: $(OBJ_DIR)/main2.o $(OBJ_DIR)/calculo_MDC.o

	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construcao do objeto main2.o:
$(OBJ_DIR)/main2.o: $(SRC_DIR)/Questao_2/main2.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto calculo_MDC.o:
$(OBJ_DIR)/calculo_MDC.o: $(SRC_DIR)/Questao_2/calculo_MDC.cpp $(INC_DIR)/Questao_2/calculo_MDC.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do executavel Questao_3:
dec2bin: $(OBJ_DIR)/main3.o $(OBJ_DIR)/conversao_binaria.o

	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construcao do objeto main3.o:
$(OBJ_DIR)/main3.o: $(SRC_DIR)/Questao_3/main3.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto conversao_binaria.o:
$(OBJ_DIR)/conversao_binaria.o: $(SRC_DIR)/Questao_3/conversao_binaria.cpp	 $(INC_DIR)/Questao_3/conversao_binaria.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do executavel Questao_4:
palindromo: $(OBJ_DIR)/main4.o $(OBJ_DIR)/palindromo.o

	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construcao do objeto main4.o:
$(OBJ_DIR)/main4.o: $(SRC_DIR)/Questao_4/main4.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto palindromo.o:
$(OBJ_DIR)/palindromo.o: $(SRC_DIR)/Questao_4/palindromo.cpp $(INC_DIR)/Questao_4/palindromo.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do executavel quadrado_Iterativo (Questão 5):
quadrado_iterativo: $(OBJ_DIR)/main5.1.o $(OBJ_DIR)/calculo_quadrado.o

	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construcao do objeto main5.1.o:
$(OBJ_DIR)/main5.1.o: $(SRC_DIR)/Questao_5/Iterativo/main5.1.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto calculo_quadrado.o:
$(OBJ_DIR)/calculo_quadrado.o: $(SRC_DIR)/Questao_5/Iterativo/calculo_quadrado.cpp $(INC_DIR)/Questao_5/Iterativo/calculo_quadrado.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do executavel quadrado_recursivo (Questão 5):
quadrado_recursivo: $(OBJ_DIR)/main5.2.o $(OBJ_DIR)/imprime.o $(OBJ_DIR)/quadrado_de_n.o

	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construcao do objeto main5.2.o:
$(OBJ_DIR)/main5.2.o: $(SRC_DIR)/Questao_5/Recursivo/main5.2.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto imprime.o:
$(OBJ_DIR)/imprime.o: $(SRC_DIR)/Questao_5/Recursivo/imprime.cpp $(INC_DIR)/Questao_5/Recursivo/imprime.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do objeto quadrado_de_n.o:
$(OBJ_DIR)/quadrado_de_n.o: $(SRC_DIR)/Questao_5/Recursivo/quadrado_de_n.cpp $(INC_DIR)/Questao_5/Recursivo/quadrado_de_n.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construcao do executavel Questão 6:
ternaria: $(OBJ_DIR)/main6.o $(OBJ_DIR)/busca_ternaria.o

	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a contrução do executavel main6.o:
$(OBJ_DIR)/main6.o: $(SRC_DIR)/Questao_6/main6.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução do executavel busca_ternária.o:
$(OBJ_DIR)/busca_ternaria.o: $(SRC_DIR)/Questao_6/busca_ternaria.cpp $(INC_DIR)/Questao_6/busca_ternaria.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a geração automatica de documentacao usando o Doxygen:
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile
doxyg:
	doxygen -g

# Alvo usado para limpar os arquivos temporarios (objeto):
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
